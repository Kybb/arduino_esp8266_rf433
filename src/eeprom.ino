//==========================================================
//                вспомогательные функции
//==========================================================
// пишем long int (32 бита) в eeprom, которая может хранить только 8 бит в одной ячейке
// для этого разбиваем число на 4 блока по 8 бит
void EEPROMWritelong(uint8_t address, long value) {
    address = address*4;
    byte four = (value & 0xFF);
    byte three = ((value >> 8) & 0xFF);
    byte two = ((value >> 16) & 0xFF);
    byte one = ((value >> 24) & 0xFF);

    //Write the 4 bytes into the eeprom memory.
    EEPROM.write(address, four);
    EEPROM.write(address + 1, three);
    EEPROM.write(address + 2, two);
    EEPROM.write(address + 3, one);
    /*
    Serial.println("EEPROMWritelong");
    Serial.println(four);
    Serial.println(three);
    Serial.println(two);
    Serial.println(one);
    Serial.println("EEEEEEEEEEEEEE");
    */
}

// считываем из eeprom 4 блока и собираем из него long int (32 бита)
//long EEPROMReadlong(long address) {
long EEPROMReadlong(uint8_t address) {
    //Read the 4 bytes from the eeprom memory.
    address = address*4;
    long four = EEPROM.read(address);       // @ нафига тут long
    long three = EEPROM.read(address + 1);
    long two = EEPROM.read(address + 2);
    long one = EEPROM.read(address + 3);

    //Return the recomposed long by using bitshift.
    return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
    // вернет 4294967295, если eeprom чистая и было записано 255 в каждой ячейке
}