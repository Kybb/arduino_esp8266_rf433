#include "main.h"

//#########################################################
//###                    Функции                        ###
//#########################################################

// функция чтения данных из UART до символа '\n'(перевод строки) и запись их в массив receivedChars[]
void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
 
  while (Serial.available() > 0 && newData == false) {    // пока есть данные в UART и висит флаг о наличии новых данных
    rc = Serial.read();                                   // читаем в rc побайно

    if (rc != endMarker) {                                // пока не "словили" символ конца строки
      receivedChars[ndx] = rc;                            // записываем в массив
      ndx++;
      if (ndx >= numChars) {                              // если размер сообщения больше, чем заданный размер(numChars) массива, то перезаписываем последний элемент массива
        ndx = numChars - 1;
      }
    }
    else {
      receivedChars[ndx] = '\0';                          // последний элемент будет '\0'
      ndx = 0;                                            // обнуляем счетчик массива
      newData = true;                                     // поднимаем флаг наличия новых данных
    }
  }
}

// парсим данные, что пришли по UART, например от ESP(прочесть eeprom, начать BIND, стереть eeprom, переключить реле)
void uartParce()  {
   
    recvWithEndMarker();

    // действия для команды read - чтение eeprom. Читаем десятичное значение ключа и номер реле
    //if (  (strcmp(receivedChars, EEPROM_READ) == 0)  && newData  )  {
    if ( (receivedChars[0] == 'r') && (receivedChars[1] == 'e') && (receivedChars[2]=='a') && (receivedChars[3]=='d') && newData )  {
        Serial.println("GET read eeprom");
        //for ( i=0; i<20; i++ ) {
        for ( i=0; i<10; i++ ) {
            keys[i] = EEPROMReadlong(i);  
            Serial.print("KEY #");
            Serial.print(i);
            Serial.print(" = ");
            Serial.print(keys[i]);
            Serial.print(" relay  ");
            Serial.println(EEPROM.read(100+i));
            delay(100);
        }
        newData = false;        // снимаем флаг, т.к. данные из UART были обработаны
    }

    // действия для команды clre - стирание/отчистка eeprom
    //if (  (strcmp(receivedChars, EEPROM_CLEAR) == 0)  && newData  )  {
    if ( (receivedChars[0] == 'c') && (receivedChars[1] == 'l') && (receivedChars[2]=='e') && (receivedChars[3]=='r') && newData )  {
        // чистим eeprom
        Serial.println("GET clear eeprom");
        for (int i = 0 ; i < EEPROM.length() ; i++) {
          EEPROM.write(i, 255);
        }
        Serial.println("eeprom cleared");
        newData = false;
    }

    // действия для команды bnAB - биндинг, А - номер записи кода кнопки в eeprom, В - номер канала реле
    if ( (receivedChars[0] == 'b') && (receivedChars[1] == 'n') && newData && ( receivedChars[3]=='0' || receivedChars[3]=='1' ) )  {
        bind = 1;   // поднимаем флаг для бесконечного цикла в котором будем ждать кнопку от пульта RF433
        Serial.println("START BINDING");
        // перед заходом в цикл сверяем время
        now = millis();                       // Возвращает количество миллисекунд с момента начала выполнения текущей программы. сбрасывается в 0 по переполнению(max=4294967296)
        if ( now - lastMsg < 0 ) lastMsg = 0;  // сброс при переполнении millis() сработает и сбросит lastMsg, который будет обчень большим. @ можно проверить только через 50 дней работы
        lastMsg = now; 
        while ( bind && (now - lastMsg < BIND_TIMEOUT) )  {             // ждем нажатия кнопки на пульте 433 или переполнения таймаута
            now = millis(); 
            if ( mySwitch.available() )  {                              // если пояилось что-то на радиоприёмнике
                rcValue = mySwitch.getReceivedValue();                  // считываем данные с RF
                // не забываем привести символы из char в int
                newKeyAddr = int(receivedChars[2])-48;
                EEPROMWritelong(newKeyAddr, rcValue);                     // адрес в eeprom для записи значения кнопки и значене кнопки
                EEPROM.write(newKeyAddr+100, int(receivedChars[3])-48);   // второй список для соответсвия с каналом реле, начинается с 100 адреса в eeprom
                
                keys[newKeyAddr] = rcValue;                               // переписываем новую кнопку в массив ключей // @ перепроверить корректность
                relays[newKeyAddr] = int(receivedChars[3])-48;            // так же обновляем номер реле

                bind = 0;                                                 // снимаем флаг, т.к. бинд произошел, что бы не ждать в условии таймаута
            }
        }
        if(!bind) Serial.println("BIND OK");  // пишем, что бинд прошел успешно
        if(bind) Serial.println("BIND TIMEOUT ERR");  // не успели забиндится за 10 секунд
        newData = false;
    }

    // обработка команд на переключение реле 0|1, ждем вида "rel00#CR#LF"
    if ( (receivedChars[0] == 'r') && (receivedChars[1] == 'e')  && (receivedChars[2] == 'l') && (receivedChars[6] == '\0') && newData )  {
        if ( receivedChars[3]=='0' ) {    // если реле №0
            if ( receivedChars[4]=='0' )  { relayState[0] = 0; digitalWrite(RELAY0, relayState[0]); Serial.println("rel00"); }
            if ( receivedChars[4]=='1' )  { relayState[0] = 1; digitalWrite(RELAY0, relayState[0]); Serial.println("rel01"); }
            newData = false;
        }

        if ( receivedChars[3]=='1' ) {    // если реле №1
            if ( receivedChars[4]=='0' )  { relayState[1] = 0; digitalWrite(RELAY1, relayState[1]); Serial.println("rel10"); }
            if ( receivedChars[4]=='1' )  { relayState[1] = 1; digitalWrite(RELAY1, relayState[1]); Serial.println("rel11"); }
            newData = false;
        }
    }

    newData = false;    // снимаем флаг, т.к. данные из UART не совпали ни с одним из условий и прибиты по несоответсвию
                        // @ возможно стоит выкинуть error в MQTT
}