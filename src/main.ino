// вторая версия логики, с обработкой нажатия только после отпускания кнопки на пульте
//#########################################################
//###                     INCLUDE                       ###
//#########################################################
#include <EEPROM.h>
#include <RCSwitch.h>       // библиотека для RF433
#include "main.h"


//#########################################################
//###                  initialization                   ###
//#########################################################
RCSwitch mySwitch = RCSwitch();   // инициализируем библиотеку RF433


//#########################################################
//###                       SETUP                       ###
//#########################################################
void setup() {

    // инициируем ножки управления реле
    pinMode(RELAY0, OUTPUT);    digitalWrite(RELAY0, LOW);  //digitalWrite(RELAY0, relayState[0]);  - если писать значние в eeprom, то при пропаже и возобновлении питания статус реле будет восстановлен.
    pinMode(RELAY1, OUTPUT);    digitalWrite(RELAY1, LOW);  //digitalWrite(RELAY1, relayState[1]);        

    // инициализация пина/прерывания для RF433 приёмника
    mySwitch.enableReceive(0);              // Receiver on interrupt 0 => that is pin #2

    Serial.begin(9600);
    delay(100);
    #if DEBUG_SERIAL 
        Serial.println("Arduino start");
    #endif 

    // на старте считываем код 10-ти кнопок из eeprom
    //for ( i=0; i<20; i++ ) {
    for ( i=0; i<10; i++ ) {
        keys[i] = EEPROMReadlong(i);  
        relays[i] = EEPROM.read(100+i);
        //delay(100);

        #if DEBUG_SERIAL 
            Serial.print("EEPROM KEY #");
            Serial.print(i);
            Serial.print(" = ");
            Serial.print(keys[i]);
            Serial.print("  RELAY #");
            Serial.println(relays[i]);
        #endif             
    }

    #if DEBUG_SERIAL 
        Serial.println("========setup end======== ");
        Serial.println();
        Serial.println();
    #endif  
}
// END SETUP
//----------------------------------------------------------------


//#########################################################
//###                       LOOP                        ###
//#########################################################
void loop() {
    
    now = millis();                               // обновляем значение в каждом цикле, для контроля времени между нажатой и отжатой кнопкой

    if (mySwitch.available()) {                   // если получили какую-либо посылку от радиомодуля
        rcValue = mySwitch.getReceivedValue();    // конвертируем в число в десятичной системе, если !0, то поднят флаг
        mySwitch.resetAvailable();                // сбрасываем флаг доступности посылки от радиомодуля
        key_pressed_flag = 1;                     // поднимаем флаг, что кнопка вжатая
        #if DEBUG_SERIAL
            Serial.println("Кнопка нажата");
        #endif
        delay(100);                               // без задержки не коректно работает сброс досутпности в библиотеке
    }else{  
            if (now - lastMsg < 0 ) lastMsg = 0;  // при переполнении millis() сработает и сбросит lastMsg, который будет обчень большим. @ можно проверить только через 50 дней работы            
            if (now - lastMsg > KEY_BUG)  {       // антидребезг RF кнопки
                    lastMsg = now;
                    key_pressed_flag = 0;         // опускаем флаг, кнопка не нажата
                    #if DEBUG_SERIAL
                        Serial.println("Кнопка отпущена");
                    #endif
            }
    }

    // если кнопка отпущена и время выполнения программы больше задержки лага и у нас есть какая-то посылка
    if( !key_pressed_flag && rcValue )   {
        #if DEBUG_RAW      // вывод сырых данных из RF модуля, вне зависимости забиндена кнопка или нет и кнопка это или просто помеха                        
            if( rcValue )   {
                Serial.print("RF = ");
                Serial.println(rcValue);
            }
        #endif

        for ( i=0; i<10; i++ ) {    // перебираем известные ключи и сравниваем с полученной посылкой
            if( (rcValue == keys[i]) ) {  // если полученная посылка совпадает с одной из известных посылок

                if ( relays[i] == 0 ) {                                                             // если кнопка относится к реле 0
                    relayState[0] = !relayState[0];                                                 // инвертируем запись о состоянии реле
                    digitalWrite(RELAY0, relayState[0]);                                            // инвертируем состояние реле
                    if ( relayState[0] )  Serial.println("rel01"); else Serial.println("rel00");    // отправляем в ESP текущее состояние реле 0
                    rcValue = 0;
                    //delay(500);
                }

                if ( relays[i] == 1 ) {                                                             // если кнопка относится к реле 1
                    relayState[1] = !relayState[1];
                    digitalWrite(RELAY1, relayState[1]);                                            // @ была ошибка, присваливался статус реле 0, а не 1
                    if ( relayState[1] ) Serial.println("rel11"); else Serial.println("rel10");
                    rcValue = 0;
                    //delay(500);
                }

                #if DEBUG_SERIAL
                    Serial.print("Pressed KEY");
                    Serial.println(i);
                #endif
            }
        }
        rcValue = 0;    // если полученный код нам не известен, то обнуляем, что бы снова безполезно не входить в цикл
    }

    uartParce();
 
}
// END LOOP
//----------------------------------------------------------------